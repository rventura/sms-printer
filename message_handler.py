# coding: utf-8
import datetime as dt
import re
import struct
from xml.dom import minidom

from pylatexenc.latexencode import UnicodeToLatexEncoder


def codepoint_to_latex(match):
    x = match.group(0)
    return "\codepoint{{{}}}".format(x.replace("|", ""))


def get_code_point(uc):
    return "|U+{}|".format(hex(ord(uc))[2:].upper())


def handle_emoji(match):
    c1, c2 = match.group(0)[:-1].replace("&#", "").split(";")
    bytecode = b"".join(struct.pack("H", val) for val in [int(c1), int(c2)])
    return get_code_point(bytecode.decode("utf-16"))


def normalize_silence_string(st):
    # Punctuation
    st = st.replace("&#10;", "\n").replace("&apos;", "'").replace("&quot;", "'")
    st = st.replace("&#0", "")
    # Emojis
    em_compile = re.compile("&#5535[0-9];&#[0-9]+;")
    lines = em_compile.sub(handle_emoji, st)
    return lines


class MessageHandler:
    def __init__(self, source, type_=None):
        """A message database parser."""
        if isinstance(source, str):
            self.messages = self.load_from_file(source, type_)
        elif isinstance(source, MessageHandler):
            self.messages = source.messages.copy()

    def load_from_file(self, input_file, type_):
        with open(input_file) as f:
            raw_text = f.read()

        if type_ == "silence":
            norm_text = normalize_silence_string(raw_text)
            xmldoc = minidom.parseString(norm_text)
            messages = [
                dict(sms.attributes.items())
                for sms in xmldoc.getElementsByTagName("sms")
            ]
            for msg in messages:
                msg["date"] = dt.datetime.fromtimestamp(int(msg["date"]) / 1000)
                msg["direction"] = "sent" if msg.get("type") == "2" else "received"
        elif type_ == "whatsapp":
            messages = []
            msg_ix = []
            date_rc = re.compile("[0-3][0-9]/[0-1][1-9]")
            for it in date_rc.finditer(raw_text):
                msg_ix.append(it.start())

            for start, end in zip(msg_ix[:-1], msg_ix[1:]):
                txt_msg = raw_text[start:end]
                date_str, tmp_str = txt_msg.split(" - ", 1)
                expedient, body = tmp_str.split(": ", 1)

                msg = {}
                msg["date"] = dt.datetime.strptime(date_str, "%d/%m/%Y à %H:%M")
                msg["body"] = body
                msg["expedient"] = expedient
                msg["direction"] = "sent" if expedient == "Raphaël" else "received"
                messages.append(msg)
        return messages

    def filter_by_number(self, number):
        new_handler = MessageHandler(self)
        new_handler.messages = [
            d for d in new_handler.messages if d.get("address") == number
        ]
        return new_handler

    def sort(self, sort_key="date"):
        self.messages = sorted(self.messages, key=lambda x: x.get(sort_key))

    def export(self, out_file):
        # Sort by date
        self.sort()

        # Latex encoder
        u2le = UnicodeToLatexEncoder(unknown_char_policy=get_code_point)
        ucp_compile = re.compile("\|U\+[0-9A-Fa-f]+\|")

        # Parse SMS
        export_string = []
        last_direction = None
        last_dt = dt.datetime(1970, 1, 1)
        for msg in self.messages:
            # Handle unicode and emojis
            body = u2le.unicode_to_latex(msg.get("body"))
            body = ucp_compile.sub(codepoint_to_latex, body)
            # Print new day
            if last_dt.date() != msg["date"].date():
                export_string.append(
                    "\pdate{{{}}}".format(msg["date"].strftime("%Y-%m-%d"))
                )
            # Potentially print hour
            if (msg["date"] - last_dt).seconds < 180 and last_direction == msg[
                "direction"
            ]:
                export_string.append("\{}{{{}}}".format(msg["direction"], body))
            else:
                export_string.append(
                    "\{}{{{}}}{{{}}}".format(
                        msg["direction"] + "hour",
                        msg["date"].strftime("{%H}{%M}{%S}"),
                        body,
                    )
                )
            last_dt = msg["date"]
            last_direction = msg["direction"]
        export_string = "\n".join(export_string)

        with open(out_file, "w") as f:
            f.write(export_string)
