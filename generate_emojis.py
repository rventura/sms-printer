import argparse
import base64
import json
import os


def main(source_file, output_dir):
    emoji_data = json.load(open(source_file))

    for emoji in emoji_data:
        codepoints = emoji[0].split()
        data = emoji[1].split(",", 1)[1]
        for codepoint in codepoints:
            with open(os.path.join(output_dir, codepoint + ".png"), "wb") as f:
                f.write(base64.standard_b64decode(data))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Generate emojis png images from json input data."
    )
    parser.add_argument("input")
    parser.add_argument(
        "-o",
        "--output",
        help="The output folder. Defaults to current directory",
        default="./",
    )
    args = parser.parse_args()
    main(args.input, args.output)
